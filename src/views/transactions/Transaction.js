import React, { useState } from 'react';
import styled from 'styled-components';
import SmallButton from '../../components/SmallButton';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-regular-svg-icons';

import {
    COLOR_BUTTON_ACTION,
    COLOR_BUTTON_ACTION_ACTIVE,
    COLOR_BUTTON_DANGER,
    COLOR_BUTTON_DANGER_ACTIVE
} from '../../colors';

const Container = styled.div`
    padding: 5px 35px;
    display: flex;
    flex-direction: column;
    :nth-child(odd) {
        background-color: rgba(255, 255, 255, 0.5);
    }

    :hover {
        /* background-color: red; */
    }
`;

const Row = styled.div`
    display: flex;
    align-items: center;
    flex-direction: row;
    padding: 5px 0;
`;

const Title = styled.div`
    flex-grow: 1;
`;

const Created = styled.div`
    flex-grow: 1;
    color: #888;
`;

const EditButton = styled(SmallButton)`
    font-size: 15px;
    background-color: ${COLOR_BUTTON_ACTION};

    :active {
        background-color: ${COLOR_BUTTON_ACTION_ACTIVE};
    }
`;

const RemoveButton = styled(SmallButton)`
    margin-left: 10px;
    font-size: 15px;
    background-color: ${COLOR_BUTTON_DANGER};

    :active {
        background-color: ${COLOR_BUTTON_DANGER_ACTIVE};
    }
`;

const Transaction = ({ name, value, type, created }) => {
    const [showDetail, setShowDetail] = useState(false);

    return (
        <>
            <Container onClick={() => setShowDetail(!showDetail)}>
                <Row>
                    <Title>{name}</Title>
                    <div>
                        {type === 'prijem' ? '+' : '-'}
                        {value}
                    </div>
                </Row>

                {showDetail ? (
                    <Row>
                        <Created>{created}</Created>
                        <EditButton>
                            <FontAwesomeIcon icon={faEdit} />
                        </EditButton>
                        <RemoveButton>
                            <FontAwesomeIcon icon={faTrashAlt} />
                        </RemoveButton>
                    </Row>
                ) : null}
            </Container>
        </>
    );
};

export default Transaction;
