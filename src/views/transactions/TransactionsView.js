import React, { useState } from 'react';
import Transaction from './Transaction';

const TransactionsView = ({
    transactions,
    setTransactions,
    isFilterIncomes,
    isFilterOutcomes
}) => {
    return (
        <div>
            {transactions
                .filter(
                    transaction =>
                        (transaction.type === 'prijem' && isFilterIncomes) ||
                        (transaction.type === 'vydaj' && isFilterOutcomes)
                )
                .map(({ id, ...props }) => (
                    <Transaction key={id} {...props} />
                ))}
        </div>
    );
};

export default TransactionsView;
