// Used colors from https://www.colourlovers.com/palette/92095/Giant_Goldfish
export const COLOR_BACKGROUND = '#E0E4CC';
export const COLOR_BACKGROUND_MODAL = '#444444bd';

export const COLOR_BUTTON_MAIN = '#F38630';
export const COLOR_BUTTON_MAIN_ACTIVE = '#FA6900';

export const COLOR_BUTTON_ACTION = '#69D2E7';
export const COLOR_BUTTON_ACTION_ACTIVE = '#A7DBD8';

export const COLOR_BUTTON_DANGER = '#f35d5d';
export const COLOR_BUTTON_DANGER_ACTIVE = '#f33030';
