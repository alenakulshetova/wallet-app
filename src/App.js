import React, { useState } from 'react';
import Header from './components/Header';
import styled, { createGlobalStyle } from 'styled-components';
import { COLOR_BACKGROUND } from './colors';

import TransactionsView from './views/transactions/TransactionsView';
import OverviewView from './views/overview/OverviewView';

const GlobalStyle = createGlobalStyle`
  body {
    margin: auto;
    max-width: 555px;
    min-width: 350px;
    font-size: 18px;
    font-family: 'Montserrat', sans-serif;
    background-color: ${COLOR_BACKGROUND};
  }
`;

const initialList = [
    { id: 1, value: 800, name: 'ATM', type: 'prijem', created: 'pondeli' },
    { id: 2, value: 123, name: 'jidlo', type: 'vydaj', created: 'pondeli' },
    { id: 3, value: 234, name: 'party', type: 'vydaj', created: 'utery' },
    { id: 4, value: 534, name: 'kafe', type: 'vydaj', created: 'streda' }
];

const App = () => {
    const [transactions, setTransactions] = useState(initialList);

    const [view, setView] = useState('transactions');

    // filters
    const [isFilterIncomes, setIsFilterIncomes] = useState(true);
    const [isFilterOutcomes, setIsFilterOutcomes] = useState(true);

    return (
        <>
            <GlobalStyle />

            <Header
                view={view}
                setView={setView}
                filterModalProps={{
                    isFilterIncomes,
                    setIsFilterIncomes,
                    isFilterOutcomes,
                    setIsFilterOutcomes
                }}
            />

            {view === 'transactions' ? (
                <TransactionsView
                    transactions={transactions}
                    setTransactions={setTransactions}
                    isFilterIncomes={isFilterIncomes}
                    isFilterOutcomes={isFilterOutcomes}
                />
            ) : (
                <OverviewView transactions={transactions} />
            )}
        </>
    );
};

export default App;
