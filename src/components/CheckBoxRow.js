import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquare, faCheckSquare } from '@fortawesome/free-regular-svg-icons';

const CheckBoxRowWrapper = styled.div`
    display: flex;
`;

const EmptyButton = styled.button`
    padding: 0;
    border: 0;
    background: none;
    outline: 0;
    font-size: 16px;
    cursor: pointer;
`;

const CheckBoxButton = styled(EmptyButton)`
    padding: 5px 5px 5px 0;
`;

const CheckBoxTitle = styled(EmptyButton)`
    flex-grow: 1;
    text-align: left;
    padding-left: 10px;
`;

const CheckBoxRow = ({ isChecked, onClick, title }) => {
    return (
        <CheckBoxRowWrapper>
            <CheckBoxButton onClick={onClick}>
                <FontAwesomeIcon icon={isChecked ? faCheckSquare : faSquare} />
            </CheckBoxButton>
            <CheckBoxTitle onClick={onClick}>{title}</CheckBoxTitle>
        </CheckBoxRowWrapper>
    );
};

export default CheckBoxRow;
