import React, { useState } from 'react';
import styled from 'styled-components';
import Button from './Button';
import AddTransactionModal from './AddTransactionModal';
import FilterModal from './FilterModal';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faFilter } from '@fortawesome/free-solid-svg-icons';

const Container = styled.div`
    width: 100%;
    box-sizing: border-box;
    padding: 30px 35px 8px 35px;
`;

const Row = styled.div`
    display: flex;
    flex-direction: row;
    margin-bottom: 15px;
`;

const Title = styled.div`
    flex-grow: 1;
`;

const DoubleButtonWrapper = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    padding: 0px 10px;
`;

const DoubleButton = styled(Button)`
    width: 50%;
    font-size: 16px;
`;

const DoubleButtonLeft = styled(DoubleButton)`
    border-radius: 45px 0 0 45px;
`;

const DoubleButtonRight = styled(DoubleButton)`
    border-radius: 0 45px 45px 0;
`;

const Header = ({ view, setView, filterModalProps }) => {
    const [
        isAddTransactionModalOpened,
        setIsAddTransactionModalOpened
    ] = useState(false);

    const [isFilterModalOpened, setIsFilterModalOpened] = useState(false);

    return (
        <>
            <Container>
                <Row>
                    <Title>Muj balance</Title>
                    <div>12345 CZK</div>
                </Row>
                <Row>
                    <Button
                        onClick={() => setIsAddTransactionModalOpened(true)}
                    >
                        <FontAwesomeIcon icon={faPlus} />
                    </Button>
                    <DoubleButtonWrapper>
                        <DoubleButtonLeft
                            isActive={view === 'transactions'}
                            onClick={() => setView('transactions')}
                        >
                            Transakce
                        </DoubleButtonLeft>
                        <DoubleButtonRight
                            isActive={view === 'overview'}
                            onClick={() => setView('overview')}
                        >
                            Prehled
                        </DoubleButtonRight>
                    </DoubleButtonWrapper>

                    <Button onClick={() => setIsFilterModalOpened(true)}>
                        <FontAwesomeIcon icon={faFilter} />
                    </Button>
                </Row>
            </Container>

            {isAddTransactionModalOpened ? (
                <AddTransactionModal
                    onClose={() => setIsAddTransactionModalOpened(false)}
                />
            ) : null}

            {isFilterModalOpened ? (
                <FilterModal
                    onClose={() => setIsFilterModalOpened(false)}
                    {...filterModalProps}
                />
            ) : null}
        </>
    );
};

export default Header;
