import styled from "styled-components";
import Button from "./Button";

const SmallButton = styled(Button)`
  width: 32px;
  height: 32px;
  font-size: 16px;
`;

export default SmallButton;
