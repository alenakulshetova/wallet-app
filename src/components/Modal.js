import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import {
    COLOR_BACKGROUND,
    COLOR_BACKGROUND_MODAL,
    COLOR_BUTTON_ACTION,
    COLOR_BUTTON_ACTION_ACTIVE
} from '../colors';
import Button from './Button';

const ModalWrapper = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background-color: ${COLOR_BACKGROUND_MODAL};
`;

const ModalBody = styled.div`
    background-color: ${COLOR_BACKGROUND};
    margin: 40px auto 0 auto;
    width: 70%;
    max-width: 470px;
`;

const ModalHeading = styled.div`
    display: flex;
    padding: 15px 15px 5px 15px;
`;

const CloseButton = styled.button`
    padding: 0;
    border: 0;
    background: none;
    outline: 0;
    font-size: 16px;
    color: #888;
    margin-right: 5px;
`;

const ModalTitle = styled.div`
    flex-grow: 1;
`;

const ModalChildren = styled.div`
    padding: 10px 15px;
`;

const SubmitButton = styled(Button)`
    width: auto;
    padding: 0 15px;
    font-size: 15px;
    background-color: ${COLOR_BUTTON_ACTION};

    :active {
        background-color: ${COLOR_BUTTON_ACTION_ACTIVE};
    }
`;

const ModalFooter = styled.div`
    text-align: center;
    padding: 5px 15px 15px 15px;
`;

const Modal = ({ onClose, title, submitButtonTitle, onSubmit, children }) => {
    return (
        <ModalWrapper>
            <ModalBody>
                <ModalHeading>
                    <ModalTitle>{title}</ModalTitle>

                    <CloseButton
                        onClick={() => {
                            onClose();
                        }}
                    >
                        <FontAwesomeIcon icon={faTimes} />
                    </CloseButton>
                </ModalHeading>

                <ModalChildren>{children}</ModalChildren>
                <ModalFooter>
                    <SubmitButton
                        onClick={() => {
                            onSubmit();
                        }}
                    >
                        {submitButtonTitle}
                    </SubmitButton>
                </ModalFooter>
            </ModalBody>
        </ModalWrapper>
    );
};

export default Modal;
