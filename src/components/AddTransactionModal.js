import React, { useState } from 'react';
import styled from 'styled-components';
import Modal from './Modal';

const AddTransactionModal = ({ onClose, onAdd }) => {
    // const onClickCreateTransaction = () => {
    //   const newObject = {
    //     id: 11233,
    //     name: "jidlo",
    //     value: 123,
    //     type: "vydaj",
    //     created: "dnes"
    //   };

    //   console.log(transactionsList);
    //   setTransactionsList([...transactionsList, newObject]);
    // };

    return (
        <Modal
            title="Nova transakce"
            onClose={onClose}
            submitButtonTitle="Pridat"
        />
    );
};

export default AddTransactionModal;
