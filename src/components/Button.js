import styled from "styled-components";
import { COLOR_BUTTON_MAIN, COLOR_BUTTON_MAIN_ACTIVE } from "../colors";

const Button = styled.button`
  text-align: center;
  color: white;
  background-color: ${({ isActive }) =>
    isActive ? COLOR_BUTTON_MAIN_ACTIVE : COLOR_BUTTON_MAIN};

  border: 0;
  :focus {
    outline: 0;
  }

  :active {
    background-color: ${COLOR_BUTTON_MAIN_ACTIVE};
  }

  border-radius: 45px;
  width: 40px;
  height: 40px;
  font-size: 16px;
`;

export default Button;
