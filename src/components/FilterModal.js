import React, { useState } from 'react';
import styled from 'styled-components';
import { COLOR_BACKGROUND, COLOR_BACKGROUND_MODAL } from '../colors';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import Modal from './Modal';
import CheckBoxRow from './CheckBoxRow';

const FilterModal = ({
    onClose,
    isFilterIncomes,
    setIsFilterIncomes,
    isFilterOutcomes,
    setIsFilterOutcomes
}) => {
    return (
        <Modal
            title="Filtry"
            onClose={onClose}
            submitButtonTitle="Hotovo"
            onSubmit={onClose}
        >
            <CheckBoxRow
                title="prijmy"
                isChecked={isFilterIncomes}
                onClick={() => setIsFilterIncomes(!isFilterIncomes)}
            />
            <CheckBoxRow
                title="vydaje"
                isChecked={isFilterOutcomes}
                onClick={() => setIsFilterOutcomes(!isFilterOutcomes)}
            />
        </Modal>
    );
};

export default FilterModal;
